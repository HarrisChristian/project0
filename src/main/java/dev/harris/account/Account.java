package dev.harris.account;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

public class Account {
    private String accountNumber;
    private BigDecimal balance;
    private LocalDate dateCreated;
    private ArrayList<AccountTransaction> transactionHistory;

    public Account(String accountNumber, BigDecimal balance) throws NullPointerException, IllegalArgumentException {
	super();
	if (accountNumber == null) {
	    throw new NullPointerException("accountNumber is null.");
	} else {
	    this.accountNumber = accountNumber;
	}
	if (balance == null) {
	    throw new NullPointerException("balance is null.");
	} else if (balance.compareTo(new BigDecimal(0)) < 0) {
	    throw new IllegalArgumentException(balance.toString() + " balance should not be a negative value.");
	} else {
	    this.balance = balance;
	}
	this.dateCreated = LocalDate.now();
	this.transactionHistory = new ArrayList<>();
    }

    public Account(String accountNumber, BigDecimal balance, LocalDate dateCreated)
	    throws NullPointerException, IllegalArgumentException {
	this(accountNumber, balance);
	if (dateCreated != null) {
	    this.dateCreated = dateCreated;
	} else {
	    throw new NullPointerException("dateCreated is null.");
	}
    }

    public Account(String accountNumber, BigDecimal balance, LocalDate dateCreated,
	    ArrayList<AccountTransaction> transactionHistory) throws NullPointerException, IllegalArgumentException {
	this(accountNumber, balance, dateCreated);
	this.transactionHistory = transactionHistory;
    }

    public String getAccountNumber() {
	return this.accountNumber;
    }

    public BigDecimal getBalance() {
	return this.balance;
    }

    public LocalDate getDateCreated() {
	return this.dateCreated;
    }

    public void addAccountTransaction(AccountTransaction transaction) {
	this.transactionHistory.add(transaction);
    }

    public ArrayList<AccountTransaction> getTransactionHistory() {
	return this.transactionHistory;
    }

    public String printAccountSummary() {
	return this.accountNumber + "\t\t" + this.balance.toString() + "\t";
    }

    @Override
    public String toString() {
	return this.accountNumber + "\n" + this.balance.toString() + "\n" + this.dateCreated.toString();
    }
}
