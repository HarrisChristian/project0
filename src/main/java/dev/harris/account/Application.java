package dev.harris.account;

import java.math.BigDecimal;

public class Application {
    private final int id;
    private final String username;
    private final BigDecimal amount;
    private ApplicationStatus status;

    public Application(int id, String username, BigDecimal amount, ApplicationStatus status) {
	this.id = id;
	this.username = username;
	this.amount = amount;
	this.status = status;
    }

    public int getId() {
	return this.id;
    }

    public String getUsername() {
	return this.username;
    }

    public BigDecimal getAmount() {
	return this.amount;
    }

    public ApplicationStatus getStatus() {
	return this.status;
    }

    public void setStatus(ApplicationStatus status) {
	this.status = status;
    }
}
