package dev.harris.account;

public enum ApplicationStatus {
    PENDING(0, "Pending"), APPROVED(1, "Approved"), REJECTED(2, "Rejected");

    final int code;
    final String name;

    private ApplicationStatus(int code, String name) {
	this.code = code;
	this.name = name;
    }

    public String getName() {
	return this.name;
    }

    public static ApplicationStatus toApplicationStatus(String s) throws IllegalArgumentException {
	if (s.equalsIgnoreCase("PENDING")) {
	    return ApplicationStatus.PENDING;
	} else if (s.equalsIgnoreCase("APPROVED")) {
	    return ApplicationStatus.APPROVED;
	} else if (s.equalsIgnoreCase("REJECTED")) {
	    return ApplicationStatus.REJECTED;
	} else {
	    throw new IllegalArgumentException(s + " is not a valid application status.");
	}
    }
}
