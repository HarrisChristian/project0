package dev.harris.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;

import dev.harris.account.Account;

public interface AccountDaoInterface {

    public ArrayList<Account> getAccounts(String username);

    public boolean accountExists(String accountNumber);

    public BigDecimal getAccountBalance(String accountNumber);

    public void transferFunds(String accountFrom, String accountTo, BigDecimal amount) throws SQLException;

    public boolean ownsAccount(String accountNumber, String username);

    public void withdrawFunds(String accountNumber, BigDecimal amount) throws SQLException;

    public void depositFunds(String accountNumber, BigDecimal amount) throws SQLException;
}
