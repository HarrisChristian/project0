package dev.harris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import dev.harris.account.AccountTransaction;
import dev.harris.util.ConnectionUtil;

public class AccountTransactionDao implements AccountTransactionDaoInterface {

    private static Logger logger = Logger.getRootLogger();

    @Override
    public ArrayList<AccountTransaction> getAccountTransactions(String accountNumber) {
	logger.info("Getting transactions for account: " + accountNumber);
	final String sql = "SELECT * FROM ACCOUNT_TRANSACTION WHERE ACCOUNT_NUMBER = ?";
	final ArrayList<AccountTransaction> transactionHistory = new ArrayList<>();
	try (Connection connection = ConnectionUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, accountNumber);
	    final ResultSet resultSet = pStatement.executeQuery();
	    while (resultSet.next()) {
		transactionHistory
			.add(new AccountTransaction(resultSet.getTimestamp("TRANSACTION_TIMESTAMP").toLocalDateTime(),
				resultSet.getString("ACCOUNT_NUMBER"), resultSet.getString("DESCRIPTION"),
				resultSet.getBigDecimal("AMOUNT"), resultSet.getBigDecimal("BALANCE")));
	    }
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
	return transactionHistory;
    }
}
