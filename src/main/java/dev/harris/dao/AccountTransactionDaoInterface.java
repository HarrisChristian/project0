package dev.harris.dao;

import java.util.ArrayList;

import dev.harris.account.AccountTransaction;

public interface AccountTransactionDaoInterface {

    public ArrayList<AccountTransaction> getAccountTransactions(String accountNumber);
}
