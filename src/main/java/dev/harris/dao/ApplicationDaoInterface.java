package dev.harris.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;

import dev.harris.account.Application;

public interface ApplicationDaoInterface {

    public void addApplication(String username, BigDecimal amount);

    public ArrayList<Application> getAllPendingApplications();

    public void acceptApplication(Application application) throws SQLException;

    public void rejectApplication(Application application);
}
