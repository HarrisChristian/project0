package dev.harris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import dev.harris.util.ConnectionUtil;

public class CredentialsDao implements CredentialsDaoInterface {

    private static Logger logger = Logger.getRootLogger();

    @Override
    public boolean validateCredentials(String username, String password) {
	logger.info("Validating credentials.");
	final String sql = "SELECT * FROM CREDENTIALS WHERE USERNAME = ?";
	try (Connection connection = ConnectionUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, username);
	    final ResultSet resultSet = pStatement.executeQuery();
	    if (resultSet.next()) {
		final String result = resultSet.getString("PASSWORD");
		return (result.equals(password));
	    }
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
	return false;
    }

}
