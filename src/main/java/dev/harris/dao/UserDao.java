package dev.harris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import dev.harris.user.Customer;
import dev.harris.util.ConnectionUtil;

public class UserDao implements UserDaoInterface {

    private static Logger logger = Logger.getRootLogger();

    @Override
    public String getUserType(String username) {
	logger.info("Getting the user type for " + username);
	final String sql = "SELECT USR_TYPE FROM USR WHERE USERNAME = ?";
	String type = null;
	try (Connection connection = ConnectionUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, username);
	    final ResultSet resultSet = pStatement.executeQuery();
	    if (resultSet.next()) {
		type = resultSet.getString("USR_TYPE");
	    }
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
	return type;
    }

    @Override
    public Customer getCustomer(String username) {
	logger.info("Reconstructing the customer object for " + username);
	Customer customer = null;
	final String sql = "SELECT * FROM USR WHERE USERNAME = ?";
	try (Connection connection = ConnectionUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, username);
	    final ResultSet resultSet = pStatement.executeQuery();
	    if (resultSet.next()) {
		if (resultSet.getString("USR_TYPE").equals("CUSTOMER")) {
		    customer = new Customer(resultSet.getString("USERNAME"), resultSet.getString("FIRST_NAME"),
			    resultSet.getString("LAST_NAME"));
		}
	    }
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
	return customer;
    }
}
