package dev.harris.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.log4j.Logger;

import dev.harris.account.Account;
import dev.harris.account.AccountTransaction;
import dev.harris.account.Application;
import dev.harris.dao.AccountDao;
import dev.harris.dao.ApplicationDao;
import dev.harris.dao.UserDao;

public class EmployeeService {

    private static Logger logger = Logger.getRootLogger();
    private final Scanner scanner = new Scanner(System.in);

    public void employeeMenu() {
	logger.info("Rendering employee menu.");
	String selection = "";
	do {
	    System.out.print("1) View Applications\n2) View Accounts\n3) View All Transactions\n0) Logout\n>");
	    selection = this.scanner.nextLine().trim();
	    switch (selection) {
	    case ("1"):
		this.viewPendingApplications();
		break;
	    case ("2"):
		this.viewAccounts();
		break;
	    case ("3"):
		this.viewAllTransactions();
		break;
	    case ("0"):
		return;
	    default:
		System.out.println(selection + " is not a valid selection.");
	    }
	} while (!selection.equals("0"));
    }

    public void viewPendingApplications() {
	logger.info("Viewing pending applications.");
	final ApplicationDao applicationDao = new ApplicationDao();
	final UserDao userDao = new UserDao();
	final ArrayList<Application> pendingApplications = applicationDao.getAllPendingApplications();

	while (true) {
	    System.out.println("All Pending Transactions");
	    for (int i = 1; i <= pendingApplications.size(); i++) {
		final String fullName = userDao.getCustomer(pendingApplications.get(i - 1).getUsername()).getFullName();
		final String initialBalance = pendingApplications.get(i - 1).getAmount().toString();
		System.out.printf("%-10s%-30s%-22s\n", i + ")", fullName, initialBalance);
	    }
	    System.out.println("Select an application to process.\n0) Exit\n>");
	    if (this.scanner.hasNextInt()) {
		final String temp = this.scanner.nextLine();
		final int selection = Integer.parseInt(temp);
		if (selection == 0) {
		    return;
		} else if ((selection < 0) || (selection > pendingApplications.size())) {
		    System.out.println(selection + " is not a valid selection.");
		} else {
		    System.out.print("How would you like to process this application? (Accept/Reject)\n>");
		    final String acceptOrReject = this.scanner.nextLine().trim();
		    if (acceptOrReject.equalsIgnoreCase("ACCEPT")) {
			System.out.println("Accepting application...");
			try {
			    applicationDao.acceptApplication(pendingApplications.get(selection - 1));
			    System.out.println("Application accepted.");
			    return;
			} catch (final SQLException ex) {
			    System.out.println("Database error application failed to process.");
			}
		    } else if (acceptOrReject.equalsIgnoreCase("REJECT")) {
			System.out.println("Rejecting application...");
			applicationDao.rejectApplication(pendingApplications.get(selection - 1));
			System.out.println("Application rejected.");
			return;
		    } else {
			System.out.println(acceptOrReject + " is not a valid selection.");
		    }
		}
	    } else {
		System.out.println(this.scanner.nextLine().trim() + " is not a valid selection.");
	    }
	}
    }

    private void viewAccounts() {
	logger.info("Viewing accounts.");
	final AccountDao accountDao = new AccountDao();
	final UserDao userDao = new UserDao();
	System.out.println("Please enter a customer's username.\n>");
	final String username = this.scanner.nextLine().trim();
	if (userDao.getCustomer(username) != null) {
	    final ArrayList<Account> accounts = accountDao.getAccounts(username);
	    for (final Account account : accounts) {
		System.out.println("Transaction history for account " + account.getAccountNumber());
		System.out.printf("%-22s%-22s%-50s%-22s%-22s\n", "Date", "Time", "Description", "Amount", "Balance");
		for (final AccountTransaction accountTransaction : account.getTransactionHistory()) {
		    final String date = accountTransaction.getDateTime().toLocalDate().toString();
		    final String time = accountTransaction.getDateTime().toLocalTime().toString();
		    final String description = accountTransaction.getDescription();
		    final String amount = accountTransaction.getAmount().toString();
		    final String balance = accountTransaction.getBalance().toString();

		    System.out.printf("%-22s%-22s%-50s%-22s%-22s\n", date, time, description, amount, balance);
		}
	    }
	} else {
	    System.out.println("There is no customer with username " + username);
	}
    }

    private void viewAllTransactions() {
	logger.info("Viewing all transactions.");
	final AccountDao accountDao = new AccountDao();
	final ArrayList<Account> accounts = accountDao.getAllAccounts();
	for (final Account account : accounts) {
	    System.out.println("Transaction history for account " + account.getAccountNumber());
	    System.out.printf("%-22s%-22s%-50s%-22s%-22s\n", "Date", "Time", "Description", "Amount", "Balance");
	    for (final AccountTransaction accountTransaction : account.getTransactionHistory()) {
		final String date = accountTransaction.getDateTime().toLocalDate().toString();
		final String time = accountTransaction.getDateTime().toLocalTime().toString();
		final String description = accountTransaction.getDescription();
		final String amount = accountTransaction.getAmount().toString();
		final String balance = accountTransaction.getBalance().toString();

		System.out.printf("%-22s%-22s%-50s%-22s%-22s\n", date, time, description, amount, balance);
	    }
	}

    }
}
