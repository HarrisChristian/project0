package dev.harris.services;

import java.util.Scanner;

import org.apache.log4j.Logger;

import dev.harris.dao.CredentialsDao;
import dev.harris.dao.UserDao;
import dev.harris.system.Main;

public class LoginService {

    private static Logger logger = Logger.getRootLogger();
    private final Scanner scanner = new Scanner(System.in);
    private final CredentialsDao credentialsDao = new CredentialsDao();
    private final UserDao userDao = new UserDao();

    public String loginMenu() {
	logger.info("Rendering login menu.");
	String type = null;
	System.out.print("Please enter your username\n>");
	final String username = this.scanner.nextLine();
	System.out.print("Please enter your password\n>");
	final String password = this.scanner.nextLine();
	if (this.credentialsDao.validateCredentials(username, password)) {
	    Main.setCurrentUser(username);
	    type = this.userDao.getUserType(username);
	} else {
	    System.out.println("Invalid username or password.");
	    type = this.loginMenu();
	}
	return type;
    }
}
