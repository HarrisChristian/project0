package dev.harris.system;

import java.util.Scanner;

import org.apache.log4j.Logger;

import dev.harris.services.CustomerService;
import dev.harris.services.EmployeeService;
import dev.harris.services.LoginService;

public class Main {

    private static Logger logger = Logger.getRootLogger();
    private static String currentUser = "";
    private final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
	logger.info("Executing main.");
	final Main main = new Main();
	main.mainMenu();
	System.out.println("Application exiting...");
    }

    public static String getCurrentUser() {
	logger.info("Retrieving current user " + currentUser + ".");
	return currentUser;
    }

    public static void setCurrentUser(String cUser) {
	logger.info("Setting current user " + cUser + ".");
	currentUser = cUser;
    }

    private void mainMenu() {
	logger.info("Rendering main menu.");
	String selection = "";
	System.out.println("WELCOME TO THE BANK APP");
	do {
	    System.out.print("1) Login\n0) Exit\n>");
	    selection = this.scanner.nextLine().trim();
	    switch (selection) {
	    case ("1"):
		final LoginService loginService = new LoginService();
		final String userType = loginService.loginMenu();
		switch (userType) {
		case ("CUSTOMER"):
		    new CustomerService().customerMenu();
		    break;
		case ("EMPLOYEE"):
		    new EmployeeService().employeeMenu();
		    break;
		default:
		    System.out.println("user type not recognized.");
		}
		break;
	    case ("0"):
		return;
	    default:
		this.mainMenu();
	    }
	} while (!selection.equals("0"));
    }
}