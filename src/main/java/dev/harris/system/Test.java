package dev.harris.system;

import java.util.ArrayList;

import dev.harris.account.Application;
import dev.harris.dao.ApplicationDao;

public class Test {

    public static void main(String[] args) {
	final ApplicationDao applicationDao = new ApplicationDao();
	final ArrayList<Application> applications = applicationDao.getAllPendingApplications();
	for (final Application app : applications) {
	    System.out.println(app.getId() + " " + app.getUsername() + " " + app.getAmount() + " " + app.getStatus());
	}

    }

}
