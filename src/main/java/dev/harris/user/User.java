package dev.harris.user;

import dev.harris.util.CredentialsUtil;

public abstract class User {
    private String username;
    private String firstName;
    private String lastName;

    public User(String username, String firstName, String lastName)
	    throws NullPointerException, IllegalArgumentException {
	super();
	if (username != null) {
	    this.username = username;
	} else {
	    throw new NullPointerException("credentials is null.");
	}
	if (firstName != null) {
	    this.firstName = firstName;
	} else {
	    throw new NullPointerException("firstName is null.");
	}
	if (lastName != null) {
	    this.lastName = lastName;
	} else {
	    throw new NullPointerException("lastName is null.");
	}
    }

    public void setUsername(String username) throws NullPointerException, IllegalArgumentException {
	if (username == null) {
	    throw new NullPointerException("username is null.");
	} else if (!CredentialsUtil.usernameIsValid(username)) {
	    throw new IllegalArgumentException(username + " is not a valid username.");
	} else {
	    this.username = username;
	}
    }

    public String getUsername() {
	return this.username;
    }

    public void setFirstName(String firstName) throws NullPointerException {
	if (firstName != null) {
	    this.firstName = firstName;
	} else {
	    throw new NullPointerException("firstName is null.");
	}
    }

    public String getFirstName() {
	return this.firstName;
    }

    public void setLastName(String lastName) throws NullPointerException {
	if (lastName != null) {
	    this.lastName = lastName;
	} else {
	    throw new NullPointerException("lastName is null.");
	}
    }

    public String getLastName() {
	return this.lastName;
    }

    public String getFullName() {
	return this.firstName + " " + this.lastName;
    }
}
