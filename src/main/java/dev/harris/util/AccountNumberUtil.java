package dev.harris.util;

import org.apache.log4j.Logger;

import dev.harris.dao.AccountDao;

public class AccountNumberUtil {
    private static Logger logger = Logger.getRootLogger();
    private static AccountDao accountDao = new AccountDao();

    public static String generateAccountNumber() {
	logger.info("Generating new account number.");
	String number;
	do {
	    number = "";
	    for (int i = 0; i < 8; i++) {
		final int digit = (int) (Math.random() * 10);
		number = number + Integer.toString(digit);
	    }
	} while (accountDao.accountExists(number));
	return number;
    }

}
