package dev.harris.util;

import org.apache.log4j.Logger;

public class CredentialsUtil {

    private static Logger logger = Logger.getRootLogger();

    private static final char[] ACCEPTED_SPECIAL_CHARACTERS = { '!', '#', '$', '%', '.', ':', '?', '_', '~' };

    public static boolean usernameIsValid(String username) throws NullPointerException {
	logger.info("Validating username " + username);
	/*
	 * Usernames have to be between 5 and 25 characters. They must begin with an
	 * alphabetic character. They are case sensitive and they can only have letters,
	 * numbers and the special characters '-', '.', and '_'. They cannot have any
	 * spaces.
	 */
	if (username == null) {
	    throw new NullPointerException("username is null.");
	}
	if ((username.length() < 5) || (username.length() > 25)) {
	    return false;
	} else if (!Character.isLetter(username.charAt(0))) {
	    return false;
	} else {
	    for (int i = 1; i < username.length(); i++) {
		if (!Character.isLetter(username.charAt(i)) && !Character.isDigit(username.charAt(i))
			&& !CredentialsUtil.isAcceptedSpecialCharacter(username.charAt(i))) {
		    return false;
		}
	    }
	}
	return true;
    }

    public static boolean passwordIsValid(String password) throws NullPointerException {
	logger.info("Validating password.");
	/*
	 * Passwords have to be between 8 and 25 characters. The must contain at least 1
	 * letter, 1 number, and 1 special character from the selected characters
	 * !#$%.:?_~.
	 */
	if (password == null) {
	    throw new NullPointerException("password is null.");
	} else {
	    if ((password.length() < 8) || (password.length() > 25)) {
		return false;
	    } else {
		boolean hasLetter = false;
		boolean hasNumber = false;
		boolean hasAcceptedSpecialCharacter = false;

		for (int i = 0; i < password.length(); i++) {
		    if (Character.isLetter(password.charAt(i))) {
			hasLetter = true;
		    } else if (Character.isDigit(password.charAt(i))) {
			hasNumber = true;
		    } else if (CredentialsUtil.isAcceptedSpecialCharacter(password.charAt(i))) {
			hasAcceptedSpecialCharacter = true;
		    } else {
			return false;
		    }
		}
		if (hasLetter && hasNumber && hasAcceptedSpecialCharacter) {
		    return true;
		} else {
		    return false;
		}
	    }
	}
    }

    public static boolean isAcceptedSpecialCharacter(char c) {
	logger.info("Determining if " + c + " is an accepted special character.");
	for (final char sc : CredentialsUtil.ACCEPTED_SPECIAL_CHARACTERS) {
	    if (sc == c) {
		return true;
	    }
	}
	return false;
    }
}
